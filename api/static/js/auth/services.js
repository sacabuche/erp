define(['angular'], function (angular) {
    'use strict';

    var erpApp = angular.module('erpApp.auth_services', []);

    erpApp.service('auth', ['$http', '$location', '$interpolate',
    function ($http, $location, $interpolate) {

        var self = this;
        var SESSION_STORAGE = 'erp.auth.session';

        var api = {
            login: '/api/v1/auth/',
            logout: $interpolate('/api/v1/auth/{{ token }}')
        };

        self.session = JSON.parse(window.localStorage.getItem(SESSION_STORAGE));

        /* TODO: put in other service, to redirect when there is no login
         * and validate if the current session is still valid*/
        if ($location.path() && (!self.session)) {
            $location.path('/');
        }

        function update_auth() {
            if (self.session && self.session.token) {
                $http.defaults.headers.common['X-Token'] = self.session.token;
            }
        }

        self.login = function () {
            var data = {
                email: self.email,
                password: self.password
            };

            return $http.post(api.login, data)
            .success(function (data, status, headers, config) {
                self.password = '';
                self.session = data;
                self.session.plain_token = data.token;
                self.session.token = [data.id, data.token].join('-');
                update_auth();
                window.localStorage.setItem(SESSION_STORAGE, JSON.stringify(data));
                $location.path('/home');
            })
            .error(function (data, status, headers, config) {
                console.log(data, status, headers);
                self.logout();
            });
        };

        self.logout = function () {
            $http.delete(api.logout({token: self.session.plain_token}))
            .success(function (data, status) {
                console.log('Token deleted');
            })
            .error(function () {
                console.log('Timeout log out or just not logged');
            })
            .finally(function () {
                self.session = null;
                window.localStorage.setItem(SESSION_STORAGE, null);
                $location.path('/');
            });
        };

        update_auth();
    }]);
});
