define(['angular', './services'], function (angular) {
    'use strict';

    var controllers = angular.module('erpApp.auth_controllers', ['erpApp.auth_services']);

    return controllers
    .controller('WelcomeCtrl', ['$scope', '$location', 'auth', function ($scope, $location, auth) {
        $scope.auth = auth;
        var now = new Date();

        if (auth.session) {
            $location.path('/home');
        }
    }])

    /* Login controller */
    .controller('HomeCtrl', ['$scope', 'auth', function ($scope, auth) {
        $scope.auth = auth;
    }])

    /* Just control if menu is collapsed an other simple things */
    .controller('menuCtrl', ['$scope', 'auth', function ($scope, auth) {
        $scope.isCollapsed = true;
        $scope.auth = auth;
    }]);
});
