define([
    'angular',
    'auth/controllers',
    'auth/services',
    'angularRoute',
    'angularBootstrap'

], function (angular, auth_controllers) {
    'use strict';

    return angular.module('erpApp', [
        'ngRoute',
        'ui.bootstrap',
        'erpApp.auth_controllers'
    ]);
});
