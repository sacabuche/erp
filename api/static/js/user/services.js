define(['angular'], function (angular) {
    'use strict';

    var SESSION_ITEM = "erp.user.session";

    var erpApp = angular.module('erpApp.servicesUser', []);

    erpApp.service('user', ['$http', '$location', '$interpolate',
    function ($http, $location, $interpolate) {

        var self = this;

        var api = {
            create: '/api/v1/users/',
            list: '/api/v1/users/',
            delete: '/api/v1/users/'
        };

        self.session = JSON.parse(window.localStorage.getItem(SESSION_ITEM));

        /* TODO: put in other service, to redirect when there is no login
         * and validate if the current session is still valid*/
        if ($location.path() && (!self.session)) {
            $location.path('/');
        }
        /*Funciones de GET, POST, DELETE y PUT para Users*/

        self.create = function () {
            var data = {
                username: self.username,
                is_active: self.is_active,
                password: self.password,
                is_superuser: self.is_superuser,
                email: self.email
            };

            return $http.post(api.create, data)
            .success(function (data, status, headers){
                self.session = data;
                window.localStorage.setItem(SESSION_ITEM, JSON.stringify(data));
            })
            .error(function()
            {
                console.log("Error al crear un usuario");
                alert("No se pudo crear el usuario");
            });
        };

        self.list = function () {
            return $http.get(api.list)
            .success(function (status)
            {
                alert("Datos correctos");
            })
            .error(function()
            {
                alert("Error al consultar lista de usuarios");
            });
         };

        self.delete = function () {
            var data = {
                username: self.username
            };
            return $http.delete(api.delete, data)
            .success(function (data, status, headers)
            {
                self.session = data;
                window.localStorage.setItem(SESSION_ITEM, JSON.stringify(data));
            })
            .error(function()
            {
                console.log("Error al eliminar un usuario");
                alert("No se pudo eliminar el usuario");
            });
        };

    }]);
});
