define(['angular', 'user/servicesUser'], function (angular) {
    'use strict';

    return angular.module('erpApp.controllersUser', ['erpApp.servicesUser'])
    
    /* Manage create, edit and deletation of users, roles, permissions */
    .controller('UsersCtrl', ['$scope', 'user', function ($scope, user) {
        $scope.user = user;
    }]);
});
