define(['angular', 'app'], function(angular, app) {
    'use strict';

    return app.config(['$routeProvider', function($routeProvider) {

        $routeProvider.when('/', {
            templateUrl: '/static/partials/welcome.html',
            controller: 'WelcomeCtrl'
        });

        $routeProvider.when('/home/', {
            templateUrl: '/static/partials/home.html',
            controller: 'HomeCtrl'
        });
/*
        $routeProvider.when('/accounts', {
            templateUrl: '/static/partials/accounts.html',
            controller: 'AccountsCtrl'
        });

        $routeProvider.when('/enterprises', {
            templateUrl: '/static/partials/enterprises.html',
            controller: 'EnterprisesCtrl'
        });

        $routeProvider.when('/users', {
            templateUrl: '/static/partials/users.html',
            controller: 'UsersCtrl'
        });
*/
        $routeProvider.otherwise({redirectTo: '/'});
    }]);

});
