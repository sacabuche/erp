require.config({
    paths: {
        angular: '/static/bower_components/angular/angular',
        angularRoute: '/static/bower_components/angular-route/angular-route',
        angularBootstrap: '/static/bower_components/angular-bootstrap/ui-bootstrap-tpls',
        text: '/static/bower_components/requirejs-text/text'
    },
    shim: {
        angular: {exports: 'angular'},
        angularBootstrap: ['angular'],
        angularRoute: ['angular'],
    },
    priority: [
        'angular'
    ]
});

window.name = "NG_DEFER_BOOTSTRAP!";


require([
    'angular',
    'app',
    'routes',

    ], function(angular, app, routes) {
    'use strict';
    var $html = angular.element(document.getElementsByTagName('html')[0]);

    angular.element().ready(function () {
        angular.resumeBootstrap([app.name]);
    });
});
