from passlib.apps import custom_app_context as pwd_context


encrypt = pwd_context.encrypt
verify = pwd_context.verify
