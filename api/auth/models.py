import uuid
from datetime import datetime, timedelta
from peewee import TextField, DateTimeField, ForeignKeyField, IntegerField
from app import db

DELTA_LOGIN = timedelta(minutes=60)


class AuthError(Exception):
    pass


class Token(db.Model):
    user = ForeignKeyField(db.UserModel)
    token = TextField()
    expires = DateTimeField()
    last_epoch = IntegerField()

    class Meta:
        indexes = (
            (('user', 'token'), True),
        )

    def save(self, *args, **kwargs):
        # se agrega la fecha que expira al momento de crearse
        if not self.id:
            self.expires = datetime.utcnow() + DELTA_LOGIN
            self.token = uuid.uuid4()
            self.last_epoch = 0
        return db.Model.save(self, *args, **kwargs)

    def extend_expire(self):
        now = datetime.utcnow()
        if self.expires >= now:
            self.expires = now + DELTA_LOGIN
            self.save()

    @classmethod
    def auth_user(cls, email, password):
        same_user = db.UserModel.email == email
        try:
            user = db.UserModel.select().where(same_user).get()
        except db.UserModel.DoesNotExist:
            raise AuthError
        if user.password_is_ok(password):
            return Token.create(user=user)
        raise AuthError

    @classmethod
    def select_current(cls, token):
        parts = token.split('-')
        token_id = parts[0]
        token = '-'.join(parts[1:])
        same_token = ((cls.token == token) & (cls.id == token_id))
        return cls.select().where(same_token).join(db.UserModel, on=(
            (cls.user == db.UserModel.id) &
            (cls.expires >= datetime.utcnow())
        ))


TABLES = (
    Token,
)
