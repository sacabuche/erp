from flask import Blueprint, request
from flask.ext.restful import marshal_with, fields, Resource, Api, reqparse

from app import app

from . import models
auth_api = Blueprint('auth', __name__)
api = Api()
api.init_app(auth_api)


class Epoch(fields.Raw):
    def format(self, date):
        return date.timestamp()


token_fields = {
    'id': fields.Integer,
    'token': fields.String,
    'expires': Epoch,
    'response': fields.String,
}

token_parser = reqparse.RequestParser()
token_parser.add_argument('X-Token', type=str, required=True,
                          location='headers')


login_parser = reqparse.RequestParser()
login_parser.add_argument('email', type=str, required=True)
login_parser.add_argument('password', type=str, required=True)


class TokenList(Resource):

    uri = '/'
    fields = token_fields
    model = models.Token

    @marshal_with(fields)
    def post(self):
        values = login_parser.parse_args(request)
        try:
            return self.model.auth_user(**values), 201
        except models.AuthError:
            return {
                'response': 'bad user or password'
            }, 401

    def get(self):
        values = token_parser.parse_args(request)
        tokens = self.model.select_current(values['token'])
        return list(tokens.dicts())


class Token(Resource):
    uri = '/<string:token>'
    model = models.Token
    fields = token_fields

    @marshal_with(fields)
    def put(self, token):
        instance = self.model.get(self.model.token == token)
        instance.extend_expire()
        return instance

    @marshal_with(fields)
    def get(self, token):
        return self.model.get(
            (self.model.token == token) & (self.model.user == request.user)
        )

    def delete(self, token):
        query = self.model.delete().where(
            (self.model.token == token) & (self.model.user == request.user)
        )

        deletions = query.execute()
        if (deletions):
            return '', 204

        return {
            'error': 'Token Not found'
        }, 404

        return self.model.get()
        pass


api.add_resource(TokenList, TokenList.uri)
api.add_resource(Token, Token.uri)

app.register_blueprint(auth_api, url_prefix='/api/v1/auth')


@app.before_request
def set_user(*args, **kwargs):
    request.user = None
    # for static files
    if '/api/' not in request.path:
        return

    if '/api/v1/auth' in request.path and request.method in ('POST', 'post'):
        # just let it pass to create the token for the user
        return

    # search for the current token
    values = token_parser.parse_args(request)

    token_query = models.Token.select_current(values['X-Token'])

    try:
        token = token_query.get()
    except models.Token.DoesNotExist:
        return 'Not logged in', 403

    # attach the current user
    token.extend_expire()
    request.user = token.user
