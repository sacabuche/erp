#Main app
import app

# apps to load
import users
import auth
import roles
import enterprises
import accounts

# create tables
app.create_tables(users)
app.create_tables(roles)
app.create_tables(auth)
app.create_tables(enterprises)
app.create_tables(accounts)

if __name__ == '__main__':
    app.run()
