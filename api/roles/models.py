from peewee import CharField, TextField, ForeignKeyField

from app import db


class Role(db.Model):
    name = CharField(max_length=150, unique=True)
    description = TextField()


class Permission(db.Model):
    name = CharField(max_length=150, unique=True)
    description = TextField()


class RolePermission(db.Model):
    role = ForeignKeyField(Role)
    permission = ForeignKeyField(Permission)

    class Meta:
        indexes = (
            # One Permission by User
            (('role', 'permission'), True),
        )


class UserRole(db.Model):
    user = ForeignKeyField(db.UserModel)
    role = ForeignKeyField(Role)

    class Meta:
        indexes = (
            (('role', 'user'), True),
        )


class UserPermission(db.Model):
    user = ForeignKeyField(db.UserModel)
    permission = ForeignKeyField(Permission)

    class Meta:
        indexes = (
            (('permission', 'user'), True),
        )


TABLES = (
    Role,
    Permission,
    RolePermission,
    UserRole,
    UserPermission,
)
