from datetime import datetime

from peewee import CharField, BooleanField, TextField, ForeignKeyField
from peewee import DateTimeField

from app import db

from passlib.apps import custom_app_context as pwd_context

encrypt = pwd_context.encrypt
verify = pwd_context.verify


class Human(db.Model):
    name = CharField(max_length=500, default='')
    last_name = CharField(max_length=500, default='')
    address = TextField()
    phone = CharField(max_length=20)


class User(db.Model):
    username = CharField(max_length=100, unique=True)
    password = CharField(max_length=1000)
    email = CharField(max_length=500)
    is_active = BooleanField(default=True)
    is_superuser = BooleanField(default=False)
    created = DateTimeField()
    modified = DateTimeField()

    def set_password(self, password):
        self.password = encrypt(password)

    def save(self, *args, **kwargs):
        # Ha sido modificado o creado
        now = datetime.now()
        self.modified = now

        if not self.id:
            self.created = now
            self.set_password(self.password)

        return db.Model.save(self, *args, **kwargs)

    def change_password(self, old, new):
        if self.password_is_ok(old):
            self.set_password(new)
            return True
        return False

    def password_is_ok(self, password):
        if verify(password, self.password):
            return True
        else:
            return False


class UserHuman(db.Model):
    human = ForeignKeyField(Human, unique=True)
    user = ForeignKeyField(User, unique=True)


# Define the  User Model to be used over all the app
db.UserModel = User


TABLES = (
    Human,
    User,
    UserHuman
)
