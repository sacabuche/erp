from flask import Blueprint, request
from flask.ext.restful import marshal_with, fields, Resource, Api, reqparse

from app import app
from . import models

#from peewee import *
#from wtfpeewee.orm import model_form

users_api = Blueprint('users', __name__)
api = Api()
api.init_app(users_api)


class DateTime(fields.Raw):
    def format(self, date):
        value = {
            'year': date.year,
            'month': date.month,
            'day': date.day,
            'hour': date.hour,
            'minute': date.minute,
            'microsecond': date.microsecond,
            'weekday': date.weekday(),
        }
        return value

user_fields = {
    'username': fields.String,
    'is_active': fields.Boolean,
    'created': DateTime,
    'modified': DateTime,
    'is_superuser': fields.Boolean,
    'email': fields.String,
}

user_parser = reqparse.RequestParser()
user_parser.add_argument('username', type=str)
user_parser.add_argument('is_active', type=bool)
user_parser.add_argument('password', type=str)
user_parser.add_argument('email', type=str)
user_parser.add_argument('is_superuser', type=bool)


class UserList(Resource):
    uri = '/'
    model = models.User
    fields = user_fields

    @marshal_with(fields)
    def get(self):
        return list(self.model.select().dicts())

    @marshal_with(fields)
    def post(self):
        values = user_parser.parse_args(request)
        values = {key: value for key, value in values.items() if value}
        return self.model.create(**values), 201

#UserListForm = model_form(UserList)


class User(Resource):
    uri = '/<string:username>'
    model = models.User
    fields = user_fields

    @marshal_with(fields)
    def get(self, username):
        return self.model.get(username=username)

    @marshal_with(fields)
    def put(self, username):
        user = self.model.get(username=username)
        values = user_parser.parse_args(request)
        password = values.get('password', None)
        for key, value in values.items():
            if value:
                setattr(user, key, value)

        if password:
            user.set_password(password)
        user.save()
        return self.model.get(username=user.usernacesarinme)

    def delete(self, username):
        user = self.model.get(username=username)
        user.delete_instance()
        return '', 204

#UserForm = model_form(User)

api.add_resource(UserList, UserList.uri)
api.add_resource(User, User.uri)

app.register_blueprint(users_api, url_prefix='/api/v1/users')
