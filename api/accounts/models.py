from peewee import ForeignKeyField, CharField, TextField
from peewee import DecimalField, DateTimeField

from app import db

from enterprises.models import Enterprise


class Account(db.Model):
    name = CharField(max_length=60)
    number = CharField(max_length=40, default='')
    enterprise = ForeignKeyField(Enterprise)
    extra_info = TextField()

    class Meta:
        indexes = (
            (('enterprise', 'name'), True),
        )


class MovementType(db.Model):
    name = CharField(max_length=60)
    description = TextField(default='')

    class Meta:
        indexes = (
            (('name',), True),
        )


class MovementStatus(db.Model):
    name = CharField(max_length=60)
    description = TextField(default='')

    class Meta:
        indexes = (
            (('name',), True),
        )


class Movement(db.Model):
    money = DecimalField(max_digits=14, decimal_places=3)
    from_account = ForeignKeyField(Account, null=True,
                                   related_name='from_movements')
    to_account = ForeignKeyField(Account, null=True,
                                 related_name='to_movements')
    type = ForeignKeyField(MovementType)
    status = ForeignKeyField(MovementStatus)
    created = DateTimeField()
    changed = DateTimeField()
    last_modified_by = ForeignKeyField(db.UserModel)
    description = TextField(default='')


class Checkpoint(db.Model):
    account = ForeignKeyField(Account)
    account_quantity = DecimalField(max_digits=14, decimal_places=3)
    today_quantity = DecimalField(max_digits=14, decimal_places=3)
    future_quantity = DecimalField(max_digits=14, decimal_places=3)


class CheckpointMovements(db.Model):
    movement = ForeignKeyField(Movement)
    checkpoint = ForeignKeyField(Checkpoint)

    class Meta:
        # Movement can't be repeated because that means that it will
        # counted many times, so we enforce that it apears only once
        indexes = (
            (('movement',), True),
        )


TABLES = (
    Account,
    MovementType,
    MovementStatus,
    Movement,
    Checkpoint,
    CheckpointMovements,
)
