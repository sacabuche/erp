from peewee import CharField, TextField, IntegerField, BooleanField
from app import db


class Enterprise(db.Model):
    name = TextField(unique=True)
    RFC = CharField(max_length=20, unique=True)
    address = TextField(default='')
    status = IntegerField(default=0)
    s = BooleanField(default=True)


TABLES = (
    Enterprise,
)
