from unipath import Path


class Configuration(object):

    DATABASE = {
        'engine': 'peewee.PostgresqlDatabase',
        'name': 'erp',
        'password': 'hola',
        'host': 'localhost',
        'user': 'daniel',
    }

    current_dir = Path(__file__).parent
    BASE_DIR = current_dir.parent
    STATIC_FOLDER = BASE_DIR.child('static')
    DEBUG = True
    SECRET_KEY = 'some deep seecret'
