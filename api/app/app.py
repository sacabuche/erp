from flask import Flask
from flask_peewee.db import Database

from .config import Configuration

app = Flask(__name__, static_url_path='',
            static_folder=Configuration.STATIC_FOLDER)
app.config.from_object(Configuration)

db = Database(app)


@app.route('/')
def root():
    return app.send_static_file('index.html')


@app.route('/static/<path:filename>')
def send_static(filename):
    return app.send_static_file(filename)
