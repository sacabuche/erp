def create_tables(module, fail_silently=True):
    try:
        models = module.models.TABLES
    except AttributeError as e:
        print(e)
        print('No TABLES for ', module.__name__)
        return

    for model in models:
        model.create_table(fail_silently=fail_silently)
